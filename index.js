
fetch('https://jsonplaceholder.typicode.com/todos')
  .then(response => response.json())
  .then(data => console.log(data))
 

fetch('https://jsonplaceholder.typicode.com/todos')
  .then(response => response.json())
  .then(data => {
    const titles = data.map(item => item.title);
    console.log(titles);
  })


fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: 'GET',
  headers: {
    'Content-Type': 'application/json'
  }
})
  .then(response => response.json())
  .then(data => { console.log(data);  })
 


fetch('https://jsonplaceholder.typicode.com/todos/', {
  method: 'POST',
  headers:  {
    'Content-Type': 'application/json'
        },
  body: JSON.stringify({
    title: "Created to do list",
    completed: false,
    userId: 1
  })
})
  .then(response => response.json())
  .then(data => console.log(data))


fetch('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'PUT',
  headers: {
    'Content-type': 'application/json; charset=UTF-8',
  },
  body: JSON.stringify({
    id: 1,
    title: 'Updated to-do item',
    completed: true
  })
})
  .then(response => response.json())
  .then(data => console.log(data))



fetch('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'PATCH',
  headers: {
    'Content-type': 'application/json; charset=UTF-8',
    },
  body: JSON.stringify({
    completed: true,
    dateCompleted: "07/09/21",
    id: 1
  }) 
})
  .then(response => response.json())
  .then(data => console.log(data))


fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: 'DELETE'
})

